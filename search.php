<?php
	session_start();
	if (empty($_POST["khoa"])) {  
		$_SESSION['khoa'] = '';
    }
	else {
		$_SESSION['khoa'] = $_POST['khoa'];
	}
	if (empty($_POST["keytext"])) {  
		$_SESSION['keyword'] = "";  
    }
	else {
		$_SESSION['keyword'] = $_POST['keytext'];
	}
?>
<!DOCTYPE html>
<html>

<head>
<title>Day08</title>

<style>
input {
  padding: 15px 15px;
  margin: 8px 0;
  box-sizing: border-box;
}
div.b {
  background-color: white;
  padding: 0px;
  border: 2px solid indigo;
  margin: 15px;
}
div.c{
	text-align:center;
}
span.d {
  display:inline-block;
  background-color:darkslateblue;
  color:white;
  height: 20px;
  width:100px;
  padding: 15px;
  border: 1px solid indigo;
  margin: 15px;
}
div.e{
	text-align:right;
}
.button {
  background-color: darkslateblue;
  border: 1px solid indigo;
  color: white;
  margin:15px;
  width:100px;
  padding: 15px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
}
table,tr,td{
	border: 0px;
}
th {
  text-align: left;
}
</style>
</head>

<body>
	<form method = "post">
		<div class = "container">
			<span class = "d"> Phân khoa </span>
			<select name="khoa" id="khoa">
				<option <?php if($_SESSION['khoa'] == ''){echo("selected");}?>></option>
				<option <?php if($_SESSION['khoa'] == 'Khoa học máy tính'){echo("selected");}?>>Khoa học máy tính</option>
				<option <?php if($_SESSION['khoa'] == 'Khoa học dữ liệu'){echo("selected");}?>>Khoa học dữ liệu</option>
			</select>
		</div>
		
		<div class = "container">
			<span class = "d"> Từ khoá </span>
			<input type="text" id="keytext" name="keytext" value="<?php echo $_SESSION['keyword']?>">
		</div>
		<div class = "c">
			<button class = "button" type = "reset">Xoá</button>
			<button class = "button" type="submit" onclick="return saveData()">Tìm kiếm</button>
		</div>
	</form>
	<p> Số sinh viên tìm thấy: 5 </p>
	<div class = "e">
		<button class = "button"> Thêm </button>
	</div>
	<table style="width:100%">
		<tr>
			<th>No</th>
			<th>Tên sinh viên</th>
			<th>Khoa</th>
			<th>Action</th>
		</tr>
		<tr>
			<td width = "5%">1</td>
			<td width = "30%">Lâm Như Nguyệt Anh</td>
			<td width = "45%">Khoa học máy tính</td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
		<tr>
			<td width = "5%">2</td>
			<td width = "30%">Nguyễn Thanh Bình</td>
			<td width = "45%">Khoa học máy tính</td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
		<tr>
			<td width = "5%">3</td>
			<td width = "30%">Trịnh Lê Quyết Chiến</td>
			<td width = "45%">Khoa học vật liệu</td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
		<tr>
			<td width = "5%">4</td>
			<td width = "30%">Tạ Quang Dũng</td>
			<td width = "45%">Khoa học máy tính</td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
		<tr>
			<td width = "5%">5</td>
			<td width = "30%">Trần Thị Trà Giang</td>
			<td width = "45%">Khoa học vật liệu</td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>